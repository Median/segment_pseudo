\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {russian}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}\IeC {\CYRV }\IeC {\cyrv }\IeC {\cyre }\IeC {\cyrd }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyre }}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}\IeC {\CYRC }\IeC {\cyre }\IeC {\cyrl }\IeC {\cyrsftsn }}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}\IeC {\CYRP }\IeC {\cyro }\IeC {\cyrs }\IeC {\cyrt }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyro }\IeC {\cyrv }\IeC {\cyrk }\IeC {\cyra } \IeC {\cyrz }\IeC {\cyra }\IeC {\cyrd }\IeC {\cyra }\IeC {\cyrch }\IeC {\cyri }}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}\IeC {\CYRO }\IeC {\cyrp }\IeC {\cyrr }\IeC {\cyre }\IeC {\cyrd }\IeC {\cyre }\IeC {\cyrl }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrya } \IeC {\cyri }~\IeC {\cyro }\IeC {\cyrb }\IeC {\cyro }\IeC {\cyrz }\IeC {\cyrn }\IeC {\cyra }\IeC {\cyrch }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrya }}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}\IeC {\CYRO }\IeC {\cyrb }\IeC {\cyrz }\IeC {\cyro }\IeC {\cyrr } \IeC {\cyrl }\IeC {\cyri }\IeC {\cyrt }\IeC {\cyre }\IeC {\cyrr }\IeC {\cyra }\IeC {\cyrt }\IeC {\cyru }\IeC {\cyrr }\IeC {\cyrery }}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}\IeC {\CYRO }\IeC {\cyrb }\IeC {\cyrshch }\IeC {\cyri }\IeC {\cyre } \IeC {\cyrz }\IeC {\cyra }\IeC {\cyrm }\IeC {\cyre }\IeC {\cyrch }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrya }}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}VIPS}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}\IeC {\CYRV }\IeC {\cyrery }\IeC {\cyrv }\IeC {\cyro }\IeC {\cyrd }\IeC {\cyrery }}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}BoM}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.1}\IeC {\CYRV }\IeC {\cyrery }\IeC {\cyrv }\IeC {\cyro }\IeC {\cyrd }\IeC {\cyrery }}{10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}A site oriented method for segmenting web pages}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1}\IeC {\CYRV }\IeC {\cyrery }\IeC {\cyrv }\IeC {\cyro }\IeC {\cyrd }\IeC {\cyrery }}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Web Page Segmentation: A Review}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}\IeC {\CYRP }\IeC {\cyrr }\IeC {\cyra }\IeC {\cyrk }\IeC {\cyrt }\IeC {\cyri }\IeC {\cyrch }\IeC {\cyre }\IeC {\cyrs }\IeC {\cyrk }\IeC {\cyra }\IeC {\cyrya } \IeC {\cyrch }\IeC {\cyra }\IeC {\cyrs }\IeC {\cyrt }\IeC {\cyrsftsn }}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}\IeC {\CYRP }\IeC {\cyrr }\IeC {\cyre }\IeC {\cyrd }\IeC {\cyrv }\IeC {\cyra }\IeC {\cyrr }\IeC {\cyri }\IeC {\cyrt }\IeC {\cyre }\IeC {\cyrl }\IeC {\cyrsftsn }\IeC {\cyrn }\IeC {\cyrery }\IeC {\cyre } \IeC {\cyrz }\IeC {\cyra }\IeC {\cyrm }\IeC {\cyre }\IeC {\cyrch }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrya }}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}\IeC {\CYRV }\IeC {\cyrery }\IeC {\cyrb }\IeC {\cyro }\IeC {\cyrr } \IeC {\cyrya }\IeC {\cyrz }\IeC {\cyrery }\IeC {\cyrk }\IeC {\cyra }}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2}\IeC {\CYRP }\IeC {\cyrr }\IeC {\cyre }\IeC {\cyrd }\IeC {\cyrp }\IeC {\cyro }\IeC {\cyrl }\IeC {\cyro }\IeC {\cyrzh }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrya }}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.3}\IeC {\CYRV }\IeC {\cyrery }\IeC {\cyrb }\IeC {\cyro }\IeC {\cyrr } \IeC {\cyrs }\IeC {\cyra }\IeC {\cyrishrt }\IeC {\cyrt }\IeC {\cyra }}{14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.4}\IeC {\CYRT }\IeC {\cyri }\IeC {\cyrp } \IeC {\cyrs }\IeC {\cyre }\IeC {\cyrg }\IeC {\cyrm }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyrt }\IeC {\cyra }\IeC {\cyrc }\IeC {\cyri }\IeC {\cyri }}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}\IeC {\CYRI }\IeC {\cyrd }\IeC {\cyre }\IeC {\cyri } \IeC {\cyrd }\IeC {\cyrl }\IeC {\cyrya } \IeC {\cyrr }\IeC {\cyre }\IeC {\cyra }\IeC {\cyrl }\IeC {\cyri }\IeC {\cyrz }\IeC {\cyra }\IeC {\cyrc }\IeC {\cyri }\IeC {\cyri }}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}\IeC {\CYRH }\IeC {\cyro }\IeC {\cyrd } \IeC {\cyrr }\IeC {\cyra }\IeC {\cyrb }\IeC {\cyro }\IeC {\cyrt }\IeC {\cyrery }}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.1}\IeC {\CYRO }\IeC {\cyrb }\IeC {\cyrhrdsn }\IeC {\cyre }\IeC {\cyrm }\IeC {\cyrl }\IeC {\cyryu }\IeC {\cyrshch }\IeC {\cyri }\IeC {\cyrishrt } \IeC {\cyrb }\IeC {\cyrl }\IeC {\cyro }\IeC {\cyrk }}{16}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.2}\IeC {\CYRO }\IeC {\cyrb }\IeC {\cyrh }\IeC {\cyro }\IeC {\cyrd } \IeC {\cyrs }\IeC {\cyrt }\IeC {\cyrr }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrc }\IeC {\cyrery }}{16}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.3}\IeC {\CYRS }\IeC {\cyrr }\IeC {\cyra }\IeC {\cyrv }\IeC {\cyrn }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyre } \IeC {\cyrerev }\IeC {\cyrl }\IeC {\cyre }\IeC {\cyrm }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyrt }\IeC {\cyro }\IeC {\cyrv } \IeC {\cyri } \IeC {\cyrd }\IeC {\cyro }\IeC {\cyrb }\IeC {\cyra }\IeC {\cyrv }\IeC {\cyrl }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyre } \IeC {\cyrerev }\IeC {\cyrl }\IeC {\cyre }\IeC {\cyrm }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyrt }\IeC {\cyra } \IeC {\cyrv } \IeC {\cyrs }\IeC {\cyre }\IeC {\cyrg }\IeC {\cyrm }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyrt }}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}\IeC {\CYRR }\IeC {\cyre }\IeC {\cyrz }\IeC {\cyru }\IeC {\cyrl }\IeC {\cyrsftsn }\IeC {\cyrt }\IeC {\cyra }\IeC {\cyrt }\IeC {\cyrery }}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}\IeC {\CYRA }\IeC {\cyrn }\IeC {\cyra }\IeC {\cyrl }\IeC {\cyri }\IeC {\cyrz }}{21}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}\IeC {\CYRZ }\IeC {\cyra }\IeC {\cyrk }\IeC {\cyrl }\IeC {\cyryu }\IeC {\cyrch }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyre }}{22}
