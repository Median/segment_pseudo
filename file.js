function documentDim(win, doc) {
    var w, h;
    if (win)
        w = win;
    else
        win = window;
    if (doc)
        d = doc;
    else
        doc = document;
    return {w: $(doc).width(), h: $(doc).height()};
}

function getRect(obj) { //только для тегов
    if (!obj) return({x: 0, y: 0, w: 10, h: 10,r:10,b:10})
    if (obj.tagName.toUpperCase() == "BODY") {
        return {x: 0, y: 0, w: documentDim().w, h: documentDim().h,r:documentDim().w,b:documentDim().h}
    } else {
        //~ r=obj.getBoundingClientRect();
        return {x: $(obj).offset().left, y: $(obj).offset().top, w: $(obj).width(), h: $(obj).height(),r:$(obj).offset().left+$(obj).width(),b:$(obj).offset().top+$(obj).height()};
    }

}

// find path from the root
function getRootPath(elem, path=[]){
    //path.push(elem);
    if(elem.tagName == "HTML"){
        return path;
    }
    else{
        path.push(elem);
        return getRootPath(elem.parentNode, path);
    }
}

//Just colorize the page with random colours
function random_colours(elem, colour=255, flag=0, cnt=0){
    var colour_step = Math.ceil(colour / (elem.childNodes.length + 1));

    var i=0;

    for(i = 0; i < elem.childNodes.length  ; i++)
    {
        random_colours(elem.childNodes[i], colour - colour_step, !flag, cnt++);
    }
    if(elem.style)
        elem.style.background = "rgb(" + 
Math.ceil(Math.random()*255) + "," + Math.ceil(Math.random()*255) +"," + Math.ceil(Math.random()*255) + 
                                                    ")";
	console.log(colour);
}


//get all the successors
function getChildrenCount(elem, l=[]){
    var sum = 1;
    var i=0;
    for(i=0; i < elem.childNodes.length; i++)
    {
        sum = sum + getChildrenCount(elem.childNodes[i]);
    }
    return sum;
}

//Euclidean between centers
function getDistance(elem1, elem2)
{
    var rect1 = getRect(elem1), 
    rect2 = getRect(elem2);
    
    x1_mid = rect1.x + rect1.w/2; y1_mid = rect1.y + rect1.h/2;
    x2_mid = rect2.x + rect2.w/2; y2_mid = rect2.y + rect2.h/2;    
    
    res = Math.sqrt(Math.pow(x1_mid-x2_mid, 2) + Math.pow(y1_mid-y2_mid, 2));
    
    //console.log(x1_mid + ", "+y1_mid+"\t"+x2_mid+', '+y2_mid+'\t'+res);
    
    return res;
}

DISTANCE_EPS = 50
function areSimilar(elem1, elem2){
    var good=0;
    
    if(!elem1 || !elem2){
        return false;
    }
    
    if(elem1.tagName.toUpperCase() === elem2.tagName.toUpperCase()){
        good += 1;
    }
    
    if(elem1.className.toUpperCase() === elem2.className.toUpperCase()){
        good += 1;
    }

    if(getDistance(elem1, elem2) <= DISTANCE_EPS)
    {
        good += 1;
    }
    
    if(good >= 2)
        return true;
    else
        return false;
}

//some news block on yandes.ru
//cur = elem1, cmp = elem1.nextElementSibling
//while(cmp){
//    if(ar.length == 0)
//    { 
//ar.push(cur);
//    }
//    
//    if(!areSimilar(cur, cmp))
//    {
//        break;
//    }
//    ar.push(cmp);
//    cur = cmp;
//    cmp = cmp.nextElementSibling;
//}
//
//itis = true
//ar = []
//x = y = 0
//while(itis)
//{
//    el = document.elementFromPoint(x,y);
//    itis=false;
//    for(i=0; i < ar.length; i++)
//    {
//        if(areSimilar(el, ar[i]))
//        {
//                    itis = true;
//            break;
//        }
//    }
//    if(ar.length == 0)
//        itis = true;
//    if(itis)
//        ar.push(el);
//    x = x;
//    y = y + getRect(el).h+2;


//avoid adding elements for second time
function contains(list, elem){
    for(var i=0; i < list.length; i++){
        if(list[i].length){
            cont = contains(list[i], elem);
            if(cont)
                return true;
        }
        else if(list[i] == elem){
            return true;
        }
    }
    return false;
}

obj = []
function addElement(elem){
    elem.processed = "true"
    if(contains(obj, elem))
        return;
    if(obj.length == 0)
        obj.push([]);
    found = false;
    for(var i=0; i<obj.length; i++){
        similar = false;
        for(var j=0; j<obj[i].length; j++){
            similar = areSimilar(elem, obj[i][j]) ? true : false;
            if(similar)
                break;
        }
        if(similar || obj[i].length == 0){
            obj[i].push(elem);
            found = true;
            break;
        }
    }
    if(!found)
        obj.push([elem]);
    
    setLabels(elem);
}

//The process is clickable
document.onclick = function(evt) {
    addElement(evt.target)
};

// Not to navigate around the site
function foo(evt){
    if(evt)
        evt.preventDefault();
}

function preprocess(obj){
    for(var i=0; i<obj.childNodes.length; i++){
        if(!obj.childNodes[i])
            continue;
        obj.childNodes[i].onclick = foo;
        preprocess(obj.childNodes[i]);
    }
}

function setLabels(elem)
{
    path = getRootPath(elem);
    for(var i=0; i < path.length; i++)
    {
        if(elem == path[i])
            continue;

        path[i].processed = "true";
    }
}

function postprocess(elem){
    for(var k=0; k < elem.childNodes.length; k++){
        if(!(elem.childNodes[k]))
                continue;
        postprocess(elem.childNodes[k]);
    }
    if(elem.style && (!(elem.processed) || (elem.processed != "true")))
        elem.style.display = "none";
}

//----------------------------------------------------
//Paint the resulting segments

function paint(segment, colourR=255, colourG=255, colourB=255){
    for(var j=0; j < segment.length; j++){
        segment[j].style.background = "rgb(" + colourR + "," + colourG +"," + colourB + ")";
    }
}

function paintAll(list){

    for(var i = 0; i < list.length; i++)
    {
        paint(list[i], Math.ceil(Math.random()*255), Math.ceil(Math.random()*255), Math.ceil(Math.random()*255));
    }
}

